"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _templateObject = _taggedTemplateLiteral(["Hello ", " world ", ""], ["Hello ", " world ", ""]),
    _templateObject2 = _taggedTemplateLiteral(["The total is ", " (", " with tax)"], ["The total is ", " (", " with tax)"]);

var _profile = require("./profile.js");

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(fibs);

require("babel-core/register");
require("babel-polyfill");

var _ref = [12, 4, 676],
    a = _ref[0],
    b = _ref[1],
    c = _ref[2],
    d = _ref.slice(3);

console.log(a, b, c, d);

function fibs() {
    var a, b, _ref2;

    return regeneratorRuntime.wrap(function fibs$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    a = 0;
                    b = 1;

                case 2:
                    if (!true) {
                        _context.next = 10;
                        break;
                    }

                    _context.next = 5;
                    return a;

                case 5:
                    _ref2 = [b, a + b];
                    a = _ref2[0];
                    b = _ref2[1];
                    _context.next = 2;
                    break;

                case 10:
                case "end":
                    return _context.stop();
            }
        }
    }, _marked, this);
}

{
    var _fibs = fibs(),
        _fibs2 = _slicedToArray(_fibs, 6),
        _first = _fibs2[0],
        second = _fibs2[1],
        third = _fibs2[2],
        fourth = _fibs2[3],
        fifth = _fibs2[4],
        sixth = _fibs2[5];

    console.log(sixth); // 5
}

//let [foo = []] = [null || undefined];


var _foo$bar = { foo: null, bar: 2 },
    _foo$bar$foo = _foo$bar.foo,
    foo = _foo$bar$foo === undefined ? [] : _foo$bar$foo,
    bar = _foo$bar.bar;

console.log(foo);

var arr = [1, 2, 3];
var first = arr[0],
    last = arr[arr.length - 1],
    length = arr.length;

console.log(first, last, length);

function fn() {
    return "Hello World";
}

console.log("foo " + fn() + " bar");

{
    var tag = function tag(stringArr) {
        for (var _len = arguments.length, values = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            values[_key - 1] = arguments[_key];
        }

        // ...
        console.log(stringArr, values);
    };

    var _a = 5;
    var _b = 10;

    tag(_templateObject, _a + _b, _a * _b);
    // 等同于
    tag(['Hello ', ' world ', ''], 15, 50);
}
{
    var passthru = function passthru(literals) {
        console.log(literals);
        var result = '';
        var i = 0;

        while (i < literals.length) {
            result += literals[i++];
            if (i < arguments.length) {
                result += arguments[i];
            }
        }

        return result;
    };

    var total = 30;
    var msg = passthru(_templateObject2, total, total * 1.05);

    msg; // "The total is 30 (31.5 with tax)"
}
{
    var _console;

    a = function a() {};
    b = function b() {
        console.log(this.c);
    };
    c = { c: 'lll' };
    (_console = console).log.apply(_console, [1].concat([2, 3, 4], [5]));
    var map = new Map([[1, 'one'], [2, 'two'], [3, 'three']]);
    console.log([].concat(_toConsumableArray(map.keys())));
}
{
    var _console2;

    var gen = /*#__PURE__*/regeneratorRuntime.mark(function gen() {
        var i;
        return regeneratorRuntime.wrap(function gen$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.t0 = regeneratorRuntime.keys([8, 1, 10]);

                    case 1:
                        if ((_context2.t1 = _context2.t0()).done) {
                            _context2.next = 7;
                            break;
                        }

                        i = _context2.t1.value;
                        _context2.next = 5;
                        return [8, 1, 10][i];

                    case 5:
                        _context2.next = 1;
                        break;

                    case 7:
                    case "end":
                        return _context2.stop();
                }
            }
        }, gen, this);
    });

    var f = function f(x, y) {
        return { x: x, y: y };
    };

    console.log([8, 9, 10]);
    (_console2 = console).log.apply(_console2, _toConsumableArray(gen()));

    console.log(f(1, 6));

    var o = {
        method: function method() {
            return "Hello!";
        }
    };

    var _foo = 1,
        _bar = 'kkk';
    var baz = { foo: _foo, bar: _bar };
    console.log(baz);
}
{
    var _a3;

    var s = Symbol();
    var _a2 = (_a3 = {
        get foo() {
            return this._foo;
        },
        set foo(foo) {
            this._foo = foo;
        }
    }, _defineProperty(_a3, s, 'symbol value'), _defineProperty(_a3, "s", 'string s value'), _a3);
    _a2.foo = 1;
    console.log(_a2.foo);

    bar = 'bar11232';
    var ss = Symbol('bar');
    Object.defineProperty(_a2, ss, { value: 0 });
    console.log(_a2[ss]);
    console.log(_a2[s]);
    console.log(_a2.s);
    console.log(Object.getOwnPropertySymbols(_a2));
}
{
    var MyArray = function MyArray() {};

    MyArray[Symbol.iterator] = /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.next = 2;
                        return 1;

                    case 2:
                        _context3.next = 4;
                        return 2;

                    case 4:
                        _context3.next = 6;
                        return 4;

                    case 6:
                    case "end":
                        return _context3.stop();
                }
            }
        }, _callee, this);
    });
    a = _defineProperty({}, Symbol.iterator, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        return regeneratorRuntime.wrap(function _callee2$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.next = 2;
                        return 1;

                    case 2:
                        _context4.next = 4;
                        return 2;

                    case 4:
                        _context4.next = 6;
                        return 4;

                    case 6:
                    case "end":
                        return _context4.stop();
                }
            }
        }, _callee2, this);
    }));
    b = new MyArray();
    console.log([].concat(_toConsumableArray(a)));
    console.log([].concat(_toConsumableArray(MyArray)));
}

{
    console.log(Object.is({}, {}));
    var _s = new Set([]);
    var aa = {};
    a = new Map([[aa, 'fff']]);
    console.log(a.get(aa), a[aa], a[{}], a.get({}), a);
}
{
    //proxy
    var proxy = {
        a: 'hello'
    };
    var obj = new Proxy(new Function(), {
        get: function get(fun, key, receiver) {
            //console.log(arguments)
            if (!key instanceof Symbol) console.log("this is key " + key + ".");
            //return Reflect.get(target, key, receiver);
            return 'world';
        },
        construct: function construct(target, args, newTarget) {
            console.log(arguments);
            return { value: args[0] * 10 };
        }
    });

    new obj(0);
    new obj(1).value;
    console.log(proxy.a, obj.a);
}
{
    var target = new Date('2015-01-01');
    var handler = {
        get: function get(a, prop) {
            if (prop === 'getDate') {
                return a.getDate.bind(a);
            }
            return Reflect.get(a, prop);
        }
    };
    var _proxy = new Proxy(target, handler);

    console.log(_proxy.getDate()); // 1
}
{
    var timeout = function timeout(ms) {
        return new Promise(function (resolve, reject) {
            setTimeout(resolve, ms, 'done');
            //reject(ms,'failed');
        });
    };

    /*timeout(5).then((value) => {
        console.log(value);
    },(value)=>{
        console.log('failed',value)
    });*/

}
{
    var getFoo = function getFoo() {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                if (Math.random() > 0.9) {
                    resolve('success');
                } else {
                    reject('failed');
                }
            });
        });
    };

    var run = function run(generator) {
        var it = generator();

        function go(result) {
            //console.log(result.value)
            if (result.done) {
                return result.value;
            }

            return result.value.then(function (value) {
                return 'run success', value;
            }, function (error) {
                return go(it.next());
            });
        }

        console.log(go(it.next()));
    };

    console.log(221);

    var g = /*#__PURE__*/regeneratorRuntime.mark(function g() {
        var _foo2;

        return regeneratorRuntime.wrap(function g$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        _context5.prev = 0;
                        _context5.next = 3;
                        return getFoo();

                    case 3:
                        _foo2 = _context5.sent;
                        _context5.next = 9;
                        break;

                    case 6:
                        _context5.prev = 6;
                        _context5.t0 = _context5["catch"](0);

                        console.log('g() throws', _context5.t0);

                    case 9:
                    case "end":
                        return _context5.stop();
                }
            }
        }, g, this, [[0, 6]]);
    });

    run(g);
}
{
    var Point = function () {
        function Point(x, y) {
            _classCallCheck(this, Point);

            this.x = x;
            this.y = y;
        }

        _createClass(Point, null, [{
            key: "to",
            value: function to() {
                console.log(this.x, this.y);
            }
        }]);

        return Point;
    }();

    Point.x = 1;
    Point.y = 2;
    Point.to();
}
try {
    var testable = function testable(target) {
        target.isTestable = true;
    };

    var readonly = function readonly(target, name, descriptor) {
        // descriptor对象原来的值如下
        // {
        //   value: specifiedFunction,
        //   enumerable: false,
        //   configurable: true,
        //   writable: true
        // };
        descriptor.writable = false;
        return descriptor;
    };

    //    @testable


    var _MyArray = function (_Array) {
        _inherits(_MyArray, _Array);

        function _MyArray() {
            var _ref3;

            _classCallCheck(this, _MyArray);

            for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                args[_key2] = arguments[_key2];
            }

            return _possibleConstructorReturn(this, (_ref3 = _MyArray.__proto__ || Object.getPrototypeOf(_MyArray)).call.apply(_ref3, [this].concat(args)));
        }

        //@readonly


        _createClass(_MyArray, [{
            key: "aa",
            value: function aa() {
                return 11;
            }
        }]);

        return _MyArray;
    }(Array);

    var _arr = new _MyArray();
    console.log(_arr.isTestable);
} catch (e) {
    console.log(e);
}

{
    console.log(_profile.bb);
}