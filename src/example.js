//import {bb} from './profile.js'
var bb = require("./profile.js")

let [a, b, c, ...d] = [12, 4, 676]
console.log(a, b, c, d)

function* fibs() {
    let a = 0;
    let b = 1;
    while (true) {
        yield a;
        [a, b] = [b, a + b];
    }
}
console.log("------------------------------------");
console.log(bb);

return;

{
    let [first, second, third, fourth, fifth, sixth] = fibs();
    console.log(sixth) // 5
}

//let [foo = []] = [null || undefined];


let {foo = [], bar} = {foo: null, bar: 2}
console.log(foo)

let arr = [1, 2, 3];
let {0: first, [arr.length - 1]: last, length} = arr;
console.log(first, last, length)

function fn() {
    return "Hello World";
}

console.log(`foo ${fn()} bar`)

{
    let a = 5;
    let b = 10;

    tag`Hello ${ a + b } world ${ a * b }`;
// 等同于
    tag(['Hello ', ' world ', ''], 15, 50);

    function tag(stringArr, ...values) {
        // ...
        console.log(stringArr, values)
    }
}
{
    let total = 30;
    let msg = passthru`The total is ${total} (${total * 1.05} with tax)`;

    function passthru(literals) {
        console.log(literals)
        let result = '';
        let i = 0;

        while (i < literals.length) {
            result += literals[i++];
            if (i < arguments.length) {
                result += arguments[i];
            }
        }

        return result;
    }

    msg // "The total is 30 (31.5 with tax)"
}
{
    a = function () {

    }
    b = function () {
        console.log(this.c)
    }
    c = {c: 'lll'}
    console.log(1, ...[2, 3, 4], 5)
    let map = new Map([
        [1, 'one'],
        [2, 'two'],
        [3, 'three'],
    ]);
    console.log([...map.keys()])
}
{
    function* gen() {
        for (let i in [8, 1, 10]) {
            yield [8, 1, 10][i]
        }
    }

    console.log([8, 9, 10])
    console.log(...gen())

    function f(x, y) {
        return {x, y};
    }

    console.log(f(1, 6))

    const o = {
        method() {
            return "Hello!";
        }
    };

    let foo = 1, bar = 'kkk'
    const baz = {foo, bar}
    console.log(baz)
}
{
    let s = Symbol()
    let a = {
        get foo() {
            return this._foo
        },
        set foo(foo) {
            this._foo = foo
        },
        [s]: 'symbol value',
        s: 'string s value',
    }
    a.foo = 1
    console.log(a.foo)

    bar = 'bar11232';
    let ss = Symbol('bar')
    Object.defineProperty(a, ss, {value: 0})
    console.log(a[ss])
    console.log(a[s])
    console.log(a.s)
    console.log(Object.getOwnPropertySymbols(a))
}
{
    function MyArray() {

    }

    MyArray[Symbol.iterator] = function* () {
        yield 1
        yield 2
        yield 4
    }
    a = {
        // 覆盖父类 Array 的构造函数
        * [Symbol.iterator]() {
            yield 1
            yield 2
            yield 4
        }

        /*MyArray(){
            console.log('MyArray')
        }*/
        //static get [Symbol.species]() { return MyArray; }
    }
    b = new MyArray()
    console.log([...a])
    console.log([...MyArray])
}

{
    console.log(Object.is({}, {}))
    let s = new Set([])
    var aa = {}
    a = new Map([
        [aa, 'fff']
    ])
    console.log(a.get(aa), a[aa], a[{}], a.get({}), a)
}
{
    //proxy
    let proxy = {
        a: 'hello'
    }
    let obj = new Proxy(new Function(), {
        get: function (fun, key, receiver) {
            //console.log(arguments)
            if (!key instanceof Symbol)
                console.log(`this is key ${key}.`)
            //return Reflect.get(target, key, receiver);
            return 'world'
        },
        construct: function (target, args, newTarget) {
            console.log(arguments)
            return {value: args[0] * 10};
        }
    });

    new obj(0);
    (new obj(1)).value;
    console.log(proxy.a, obj.a);
}
{
    const target = new Date('2015-01-01');
    const handler = {
        get(a, prop) {
            if (prop === 'getDate') {
                return a.getDate.bind(a);
            }
            return Reflect.get(a, prop);
        }
    };
    const proxy = new Proxy(target, handler);

    console.log(proxy.getDate()) // 1
}
{
    function timeout(ms) {
        return new Promise((resolve, reject) => {
            setTimeout(resolve, ms, 'done');
            //reject(ms,'failed');
        });
    }

    /*timeout(5).then((value) => {
        console.log(value);
    },(value)=>{
        console.log('failed',value)
    });*/
}
{
    console.log(221)

    function getFoo() {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                if (Math.random() > 0.9) {
                    resolve('success');
                }
                else {
                    reject('failed')
                }
            })
        });
    }

    const g = function* () {
        try {
            const foo = yield getFoo();
        } catch (e) {
            console.log('g() throws', e);
        }
    };

    function run(generator) {
        const it = generator();

        function go(result) {
            //console.log(result.value)
            if (result.done) {
                return result.value;
            }

            return result.value.then(function (value) {
                return 'run success', value;
            }, function (error) {
                return go(it.next());
            });
        }

        console.log(go(it.next()))
    }

    run(g);
}
{
    class Point {

        constructor(x, y) {
            this.x = x;
            this.y = y;
        }

        static to() {
            console.log(this.x, this.y)
        }
    }

    Point.x = 1;
    Point.y = 2;
    Point.to()
}
try {
    function testable(target) {
        target.isTestable = true;
    }

    function readonly(target, name, descriptor) {
        // descriptor对象原来的值如下
        // {
        //   value: specifiedFunction,
        //   enumerable: false,
        //   configurable: true,
        //   writable: true
        // };
        descriptor.writable = false;
        return descriptor;
    }

  //    @testable
    class MyArray extends Array {
        constructor(...args) {
            super(...args);
        }

        //@readonly
        aa() {
            return 11;
        }
    }


    let arr = new MyArray();
    console.log(arr.isTestable)
}
catch (e) {
    console.log(e)
}



{
    console.log(bb)
}